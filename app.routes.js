(function() {

    angular.module('ServiHogApp').config(configuracion);


    configuracion.$inject = [
        '$authProvider',
        '$stateProvider',
        '$urlRouterProvider',
        '$middlewareProvider'
    ];

    function configuracion($authProvider, $stateProvider,
    	$urlRouterProvider, $middlewareProvider) {

        $middlewareProvider.map({
            'authMiddleware': ['$auth', function($auth) {

                if (!$auth.isAuthenticated()) {
                    this.redirectTo('login');
                }
                this.next();

            }],
            'authLoginMiddleware': ['$auth', function($auth) {

                if ($auth.isAuthenticated()) {
                    this.redirectTo('main');
                }

                this.next();
            }]
        });

        //var url = "http://localhost:8000/api/auth/";
        var url = "http://18.223.124.136:8000/api/auth/";

        $authProvider.loginUrl = url + "login";
        $authProvider.signupUrl = url + "registro";
        $authProvider.tokenName = "token";
        $authProvider.tokenPrefix = "ServiHogApp";

        $stateProvider
            .state('main', {
                templateUrl: 'views/main.html',
                controller: 'MainCtrl',
                controllerAs: 'main',
            })
            // Inicio de rutas Cliente
            .state('main.cliente', {
                url: '/cliente',
                templateUrl: 'views/cliente/index.html',
                middleware: 'authMiddleware',
                controller: 'ClienteCtrl',
                controllerAs: 'ctrl',
            })
            .state('main.ver_servicios', {
                url: '/ver_servicios',
                templateUrl: 'views/cliente/ver_servicios.html',
                middleware: 'authMiddleware',
                controller: 'ClienteCtrl',
                controllerAs: 'ctrl',
            })
            .state('main.perfil_cliente', {
                url: '/perfil_cliente',
                templateUrl: 'views/cliente/perfil.html',
                middleware: 'authMiddleware',
                controller: 'ClienteCtrl',
                controllerAs: 'ctrl',
            })
            .state('main.servicios_solicitados', {
                url: '/servicios_solicitados',
                templateUrl: 'views/cliente/servicios_solicitados.html',
                middleware: 'authMiddleware',
                controller: 'ClienteCtrl',
                controllerAs: 'ctrl',
            })
            .state('main.colaboradores_servicios', {
                url: '/colaboradores_servicios',
                templateUrl: 'views/cliente/colaboradores_servicios.html',
                middleware: 'authMiddleware',
                controller: 'ClienteCtrl',
                controllerAs: 'ctrl',
            })
            // Fin de rutas Cliente
            // Inicio de rutas Colaborador
            .state('main.colaborador', {
                url: '/colaborador',
                templateUrl: 'views/colaborador/index.html',
                middleware: 'authMiddleware',
                controller: 'colaboradorCtrl',
                controllerAs: 'ctrl',
            })
              // Inicio de rutas Colaborador perfi
             .state('main.perfil', {
                url: '/colaborador/perfil',
                templateUrl: 'views/colaborador/perfil.html',
                middleware: 'authMiddleware',
                controller: 'colaboradorCtrl',
                controllerAs: 'ctrl',
            })

             // Inicio de rutas Historial Colaborador
             .state('main.historialcolaborador', {
                url: '/colaborador/historico',
                templateUrl: 'views/colaborador/historico.html',
                middleware: 'authMiddleware',
                controller: 'colaboradorCtrl',
                controllerAs: 'ctrl',
            })

             // Inicio de rutas Solicitud Colaborador
             .state('main.solicitudcolaborador', {
                url: '/colaborador/solicitud',
                templateUrl: 'views/colaborador/solicitud.html',
                middleware: 'authMiddleware',
                controller: 'colaboradorCtrl',
                controllerAs: 'ctrl',
            })

            .state('main.cambiar_servicio', {
                url: '/colaborador/servicio',
                templateUrl: 'views/colaborador/servicio.html',
                middleware: 'authMiddleware',
                controller: 'colaboradorCtrl',
                controllerAs: 'ctrl',
            })

            //Inicio de rutas de Administrador
            .state('main.admin_solicitudes', {
                url: '/administrador/solicitudes',
                templateUrl: 'views/administrador/solicitudes.html',
                middleware: 'authMiddleware',
                controller: 'administradorCtrl',
                controllerAs: 'ctrl',
            })

            .state('main.ver_usuarios', {
                url: '/administrador/usuarios',
                templateUrl: 'views/administrador/usuarios.html',
                middleware: 'authMiddleware',
                controller: 'administradorCtrl',
                controllerAs: 'ctrl',
            })

            .state('main.editar_usuario', {
                url: '/administrador/editar_usuario',
                templateUrl: 'views/administrador/editar_usuario.html',
                middleware: 'authMiddleware',
                controller: 'administradorCtrl',
                controllerAs: 'ctrl',  
            })

            .state('main.ver_servicios_admin', {
                url: '/administrador/servicios',
                templateUrl: 'views/administrador/servicios.html',
                middleware: 'authMiddleware',
                controller: 'administradorCtrl',
                controllerAs: 'ctrl',  
            })

            .state('main.modificar_servicios_admin', {
                url: '/administrador/editar_servicio',
                templateUrl: 'views/administrador/editar_servicios.html',
                middleware: 'authMiddleware',
                controller: 'administradorCtrl',
                controllerAs: 'ctrl',  
            })

            // Fin de rutas Colaborador
            .state('login', {
                url: '/login',
                templateUrl: 'views/login/index.html',
                middleware: 'authLoginMiddleware',
                controller: 'LoginCtrl',
                controllerAs: 'login',
            })

            .state('registrarse', {
                url: '/registrarse',
                templateUrl: 'views/login/registrarse.html',
                middleware: 'authLoginMiddleware',
                controller: 'LoginCtrl',
                controllerAs: 'login',
            });

        $urlRouterProvider.otherwise('login');
	}

})();
