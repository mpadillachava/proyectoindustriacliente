(function(){
	angular.module('ServiHogApp')
		.factory('apiServices', ctiApi);

	ctiApi.$inject = ['$http', 'urlBase', '$auth'];

	function ctiApi ($http, urlBase, $auth){
		return{
			get :function(url){
					return $http.get(urlBase + url);
			},

			post :function(url, data){
					return $http.post(urlBase + url, data);
			},

			put :function(url, data){
				return $http.put(urlBase + url, data);
			},

			delete :function(url, data){
					return $http.delete(urlBase + url, data);
			},

			postFile:function(url, data){
				return $http.post(urlBase+url, data,
					{
						transformRequest: angular.identity,
						headers: {'Content-Type': undefined}
					})
			}
		}
	}
})();
