(function(){
	angular.module('ServiHogApp')
		.factory('initDatos', datos);

	function datos(apiServices){
		var ftry = this;

		ftry.username = "";
		ftry.rol = "";

		ftry.query = {
			order: 'name',
		    limit: 5,
		    page: 1
		};
		ftry.options = {
		    rowSelection: true,
		    multiSelect: true,
		    autoSelect: true,
		    decapitate: false,
		    largeEditDialog: false,
		    boundaryLinks: true,
		    limitSelect: true,
		    pageSelect: true
	    };
		return ftry;
	}
})();
