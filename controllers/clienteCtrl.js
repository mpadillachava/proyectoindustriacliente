(function() {
    angular.module('ServiHogApp')
        .controller('ClienteCtrl', ClienteCtrl)
        .factory('CategoriaFtry', CategoriaFtry);

    ClienteCtrl.$inject = ['$auth', '$state', 'apiServices', 'initDatos','CategoriaFtry'];

    function CategoriaFtry(){
        var ftry = this;

        ftry.categoria = {};
        ftry.servicio = {};
        ftry.datosTemp = {};

        return ftry;
    }

    function ClienteCtrl($auth, $state, apiServices, initDatos){
    	var ctrl = this;
        ctrl.categorias = [];
        ctrl.servicios = [];
        ctrl.colaboradores = [];
        ctrl.categoria_actual = '';
        ctrl.colaborador_actual = {};
    	this.init_index = function(){
            ctrl.query = initDatos.query;
            ctrl.options = initDatos.options;
            apiServices.get('categorias')
                .then((response) => {
                    if (response.data.validate) {
                        ctrl.categorias = response.data.data;
                        console.log(ctrl.categorias);
                    } else {
                        toastr.options.closeButton = true;
                        toastr.options.positionClass = "toast-bottom-center";
                        toastr.options.closeMethod = 'fadeOut';
                        toastr.warning(response.data.message, 'Error!',{timeOut: 7000});
                    }
            });
            //console.log(ctrl.categorias);
        }

        this.cambiar_estado = function(estado){
            $state.go(estado);
        }

        this.ver_servicios = function(item){
            ctrl.categoria_actual = {
                id:item.id,
                descripcion:item.descripcion,
            };
            
            CategoriaFtry.categoria = ctrl.categoria_actual;
            $state.go('main.ver_servicios');
        }

        this.init_servicios = function() {
            ctrl.categoria_actual = CategoriaFtry.categoria;
            console.log(ctrl.categoria_actual);
            ctrl.servicios = [];
            apiServices.post('obtener_servicios',ctrl.categoria_actual)
            .then((response) => {
                if (response.data.validate) {
                    ctrl.servicios = response.data.data;
                    console.log(ctrl.servicios);
                } else {
                    toastr.options.closeButton = true;
                    toastr.options.positionClass = "toast-bottom-center";
                    toastr.options.closeMethod = 'fadeOut';
                    toastr.warning(response.data.message, 'Error!',{timeOut: 7000});
                }
            });
        }

        this.colaboradores_servicios = function(item){
            
        }

        this.mostrar_colaborador_servicio = function(item){
            ctrl.servicio_actual = {
                id:item.id,
                descripcion:item.descripcion,
            };

            ctrl.servicio_actual = {
                id:item.id,
                descripcion:item.descripcion,
            };
            
            CategoriaFtry.servicio = ctrl.servicio_actual;
            $state.go('main.colaboradores_servicios');
        }

        this.init_colaboradores_servicios = function(){
            ctrl.servicio_actual = CategoriaFtry.servicio;
            apiServices.post('obtener_colaboradores',ctrl.servicio_actual)
            .then((response) => {
                if (response.data.validate) {
                    ctrl.colaboradores = response.data.data;
                    console.log(ctrl.colaboradores);
                } else {
                    toastr.options.closeButton = true;
                    toastr.options.positionClass = "toast-bottom-center";
                    toastr.options.closeMethod = 'fadeOut';
                    toastr.warning(response.data.message, 'Error!',{timeOut: 7000});
                }
            });   
        }

        this.contratar_colaborador_servicio = function(){
            ctrl.insertar_servicio = {
                descripcion_solicitud:ctrl.cc_descripcion_solicitud,
                fecha:ctrl.cc_fecha_servicio,
                colaborador:ctrl.colaborador_actual
            }
            console.log(ctrl.insertar_servicio);
            apiServices.post('contratar_colaborador',ctrl.insertar_servicio)
            .then((response) => {
                if (response.data.validate) {
                    toastr.options.closeButton = true;
                    toastr.options.positionClass = "toast-bottom-center";
                    toastr.options.closeMethod = 'fadeOut';
                    toastr.success(response.data.message, 'Exito!',{timeOut: 7000});
                } else {
                    toastr.options.closeButton = true;
                    toastr.options.positionClass = "toast-bottom-center";
                    toastr.options.closeMethod = 'fadeOut';
                    toastr.warning(response.data.message, 'Error!',{timeOut: 7000});
                }
            });
        }

        this.mostrar_contratar_colaborador = function(item){
            ctrl.colaborador_actual = item;
            $('#myModal').modal('toggle');
        }

        this.init_solicitudes = function(){
            ctrl.solicitudes = [];
            apiServices.get('solicitudes')
                .then((response) => {
                    if (response.data.validate) {
                        ctrl.solicitudes = response.data.data;
                        console.log(ctrl.solicitudes);
                    } else {
                        toastr.options.closeButton = true;
                        toastr.options.positionClass = "toast-bottom-center";
                        toastr.options.closeMethod = 'fadeOut';
                        toastr.warning(response.data.message, 'Error!',{timeOut: 7000});
                    }
            });
        }
        
    }
})();