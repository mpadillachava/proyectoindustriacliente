(function() {
    angular.module('ServiHogApp')
           .controller('administradorCtrl', administradorCtrl)
           .factory('UserFtry', UserFtry);

    administradorCtrl.$inject = [
        '$state', '$auth', 'apiServices','UserFtry'
    ];

    function UserFtry(){
        var ftry = this;

        ftry.user = {};
        ftry.servicio = {};
        ftry.datosTemp = {};

        return ftry;
    }

    function administradorCtrl($state, $auth, apiServices) {
        var ctrl = this;
        ctrl.usuarios = [];
        ctrl.servicios = [];
        this.init_usuarios = function() {
            apiServices.get('get_usuarios')
                .then((response) => {
                if (response.data.validate) {
                    ctrl.usuarios = response.data.data;
                    console.log(ctrl.usuarios);
                } else {
                    toastr.options.closeButton = true;
                    toastr.options.positionClass = "toast-bottom-center";
                    toastr.options.closeMethod = 'fadeOut';
                    toastr.warning(response.data.message, 'Error!',{timeOut: 7000});
                }
            });
        }

        this.goEditarUser = function(item){
            ctrl.user_actual = {
                apellido:item.apellido,
                email:item.email,
                fecha_inscripcion:item.fecha_inscripcion,
                foto_perfil:item.foto_perfil,
                id:item.id,
                id_rol:item.id_rol.id.toString(),
                nombre:item.nombre,
                sexo:item.sexo,
                telefono:item.telefono
            };
            if (ctrl.user_actual.foto_perfil == 'null') {
                ctrl.user_actual.foto_perfil = 'https://visualpharm.com/assets/217/Life%20Cycle-595b40b75ba036ed117d9ef0.svg';
            }
            UserFtry.user = ctrl.user_actual;
            $state.go('main.editar_usuario');
        }

        this.init_editUser = function(){
            ctrl.user_actual = UserFtry.user;
            var files = document.querySelectorAll("input[type=file]");
            files[0].addEventListener("change", function(e) {
                if(this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function(e) { document.getElementById("img_foto_perfil_edit").setAttribute("src", e.target.result); }
                    reader.readAsDataURL(this.files[0]);
                }
            });
        }

        this.actualizar_usuario = function () {
            var formData = new FormData();
            formData.append('nombre',ctrl.user_actual.nombre);
            formData.append('apellido',ctrl.user_actual.apellido);
            formData.append('sexo', ctrl.user_actual.sexo);
            formData.append('correo', ctrl.user_actual.email);
            formData.append('telefono', ctrl.user_actual.telefono);
            formData.append('id_rol', ctrl.user_actual.id_rol);
            formData.append('password', ctrl.user_actual.password);
            formData.append('foto_perfil', $('#imagen_perfil')[0].files[0]);
            formData.append('id',ctrl.user_actual.id);
            apiServices.postFile('actualizar_usuario',formData)
                .then((response) => {
                    if (response.data.validate) {
                        toastr.options.closeButton = true;
                        toastr.options.positionClass = "toast-bottom-center";
                        toastr.success(response.data.message);
                        $state.go('main.ver_usuarios');
                    } else {
                        ctrl.fails = response.data.message;
                    }
            });
        }

        this.cambiar_menu = function(estado) {
            $state.go(estado);
        }

        this.init_servicios = function(){
            apiServices.get('get_servicios')
                .then((response) => {
                if (response.data.validate) {
                    ctrl.servicios = response.data.data;
                    console.log(ctrl.servicios);
                } else {
                    toastr.options.closeButton = true;
                    toastr.options.positionClass = "toast-bottom-center";
                    toastr.options.closeMethod = 'fadeOut';
                    toastr.warning(response.data.message, 'Error!',{timeOut: 7000});
                }
            });
        }

        this.goEditarServicio = function(item){
            ctrl.servicio_actual = {
                nombre:item.nombre,
                descripcion:item.descripcion,
                servicio_domicilio:item.servicio_domicilio,
                id_categoria:item.id_categoria,
                imagen:item.imagen,
                id:item.id
            };
            if (ctrl.servicio_actual.imagen == 'null' || ctrl.servicio_actual.imagen == '') {
                ctrl.servicio_actual.imagen = 'https://visualpharm.com/assets/217/Life%20Cycle-595b40b75ba036ed117d9ef0.svg';
            }
            UserFtry.servicio = ctrl.servicio_actual;
            $state.go('main.modificar_servicios_admin');
        }

        this.init_editServicios = function(){
            ctrl.servicio_actual = UserFtry.servicio;
            
        }


    }
})();
