(function() {
    angular.module('ServiHogApp')
        .controller('colaboradorCtrl', colaboradorCtrl);

    colaboradorCtrl.$inject = ['$auth', '$state', 'apiServices', 'initDatos'];

    function colaboradorCtrl($auth, $state, apiServices, initDatos){
    	ctrl = this;
    	ctrl.solicitudes_pendientes = [];
    	ctrl.solicitudes_aprobadas = [];
    	ctrl.solicitud_actual = {};
    	this.init_solicitudes = function(){
    		apiServices.get('solicitudes_pendientes')
                .then((response) => {
                    if (response.data.validate) {
                        ctrl.solicitudes_pendientes = response.data.data;
                        console.log(ctrl.solicitudes_pendientes);
                    } else {
                        toastr.options.closeButton = true;
                        toastr.options.positionClass = "toast-bottom-center";
                        toastr.options.closeMethod = 'fadeOut';
                        toastr.warning(response.data.message, 'Error!',{timeOut: 7000});
                    }
            });
    	}

    	this.init_soli_aprobadas = function(){
    		apiServices.get('solicitudes_aprobadas')
                .then((response) => {
                    if (response.data.validate) {
                        ctrl.solicitudes_aprobadas = response.data.data;
                        console.log(ctrl.solicitudes_aprobadas);
                    } else {
                        toastr.options.closeButton = true;
                        toastr.options.positionClass = "toast-bottom-center";
                        toastr.options.closeMethod = 'fadeOut';
                        toastr.warning(response.data.message, 'Error!',{timeOut: 7000});
                    }
            });	
    	}

    	this.aceptar_solicitud = function(item){
    		ctrl.solicitud_actual = {
    			id:item.id,
    			aprobado:1
    		};
    		apiServices.post('aceptar_solicitud',ctrl.solicitud_actual)
                .then((response) => {
                    if (response.data.validate) {
                        toastr.options.closeButton = true;
                        toastr.options.positionClass = "toast-bottom-center";
                        toastr.options.closeMethod = 'fadeOut';
                        toastr.success(response.data.message, 'Exito!',{timeOut: 7000});
                        setTimeout(function(){ $state.go('main.historialcolaborador'); }, 7000);
                    } else {
                        toastr.options.closeButton = true;
                        toastr.options.positionClass = "toast-bottom-center";
                        toastr.options.closeMethod = 'fadeOut';
                        toastr.warning(response.data.message, 'Error!',{timeOut: 7000});
                    }
            });
    	}

    	this.cancelar_solicitud = function(item){
    		ctrl.solicitud_actual = {
    			id:item.id,
    			aprobado:0
    		};
    		apiServices.post('cancelar_solicitud',ctrl.solicitud_actual)
                .then((response) => {
                    if (response.data.validate) {
                        toastr.options.closeButton = true;
                        toastr.options.positionClass = "toast-bottom-center";
                        toastr.options.closeMethod = 'fadeOut';
                        toastr.success(response.data.message, 'Exito!',{timeOut: 7000});
                    } else {
                        toastr.options.closeButton = true;
                        toastr.options.positionClass = "toast-bottom-center";
                        toastr.options.closeMethod = 'fadeOut';
                        toastr.warning(response.data.message, 'Error!',{timeOut: 7000});
                    }
            });
    	}
    }
})();
