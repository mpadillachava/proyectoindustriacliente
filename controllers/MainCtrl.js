(function() {
    angular.module('ServiHogApp')
        .controller('MainCtrl', MainCtrl);

    MainCtrl.$inject = [
        '$state', '$auth', 'apiServices'
    ];

    function MainCtrl($state, $auth, apiServices) {
          var ctrl = this;
          var mRol;
          ctrl.user = {};
          ctrl.UsertoModify = {};
          apiServices.get('menu/rol')
            .then((response) => {
                if (response.data.validate) {
                    ctrl.username = response.data.username;
                    ctrl.menu = [];
                    ctrl.menu = response.data.menu;
                    mRol = response.data.rol;
                    ctrl.user = response.data.user;
                    ctrl.user.fec_nacimiento = new Date(ctrl.user.fec_nacimiento);
                    this.redirectRol(mRol);
                } else {
                    console.log("error");
                }
            });

        this.cambiar_menu = function(estado) {
            $state.go(estado);
        }
        this.mostrar_item = function(item){
            for (var i = 0; i < ctrl.menu.length; i++) {
                if (ctrl.menu[i] == item) {
                    ctrl.menu[i].mostrar = true;
                }
                else{
                    ctrl.menu[i].mostrar = false;
                }

            }
        }

       this.redirectRol = function(mRol) {
            switch (mRol) {
                case 'Cliente':
                    $state.go('main.cliente');
                    break;

                case 'Administrador':
                    $state.go('main.admin_solicitudes');
                    break;

                case 'Colaborador':
                    $state.go('main.colaborador');
                    break;
            }
       }

        this.logout = function() {
            $auth.logout();
            $state.go('login');
        }
    }
})();
