(function() {
    angular.module('ServiHogApp')
        .controller('LoginCtrl', LoginCtrl)
        .factory('LoginFtry', LoginFtry);;

    LoginCtrl.$inject = ['$auth', '$state' , '$http', 'initDatos','apiServices','LoginFtry'];

    function LoginFtry(){
        var ftry = this;

        ftry.user = {};

        return ftry;
    }

    function LoginCtrl($auth, $state, initDatos,apiServices,$http) {
        var ctrl = this;
        ctrl.show_progress = false;
        ctrl.email = "asd1@gmail.com";
        ctrl.sexo = 'M';
        ctrl.departamento = '1';
        ctrl.rol = '1';
        ctrl.ubicacion = false;
        ctrl.latitude  = 14.16;
        ctrl.longitude = -87.04;
        ctrl.imagen_mapa = '';
        this.login = function() {
            ctrl.errores = false;
            ctrl.show_progress = true;
            var credenciales = {
                email: ctrl.email,
                password: ctrl.password,
                operacion: 'login'
            };

            $auth.login(credenciales)
                .then(success)
                .catch(error);

            function success(response) {
                ctrl.show_progress = false;
                if (response.data.validate) {
                    initDatos.username = response.data.username;
                    $state.go('main');
                } else{
                    ctrl.fail = response.data.message;
                    ctrl.errores = true;
                }
            }

            function error(response) {
                ctrl.show_progress = false;
                ctrl.password = null;
                ctrl.fail = response.data.message;
                ctrl.errores = true;
            }
        }

        this.ir_registrarse = function() {
            $state.go('registrarse');
        }


        this.obtener_ubicacion = function() {
            navigator.geolocation.getCurrentPosition(function(position){
                ctrl.latitude  = position.coords.latitude;
                ctrl.longitude = position.coords.longitude;
                console.log(ctrl.longitude);
                ctrl.imagen_mapa = 'https://maps.googleapis.com/maps/api/staticmap?center='+ctrl.latitude+','+ctrl.longitude+'&zoom=15&size=580x300&maptype=hybrid&markers=color:red%7Clabel:L%7C'+ctrl.latitude+','+ctrl.longitude+'&key=AIzaSyDViis-uYnCufSm5Z7E4b24pWONT96mmY0';
            });
            console.log(ctrl.longitude);
            ctrl.ubicacion = true;
        }

        this.registrarse = function(){
            ctrl.usuario_a_insertar = {
                nombre:ctrl.nombre,
                apellido:ctrl.apellido,
                email:ctrl.email,
                telefono:ctrl.telefono,
                sexo:ctrl.sexo,
                password:ctrl.password,
                password2:ctrl.password2,
                id_rol:ctrl.rol,
                id_categoria:ctrl.categoria,
                id_servicio:ctrl.servicio,
                id_departamento:ctrl.departamento,
                colonia:ctrl.colonia,
                direccion_especifica:ctrl.direccion_especifica,
                latitud: ctrl.latitude,
                longitud: ctrl.longitude,
                operacion: 'registrarse'
            }
            console.log(ctrl.usuario_a_insertar);
            $auth.login(ctrl.usuario_a_insertar)
                .then(success)
                .catch(error);

            function success(response) {
                ctrl.show_progress = false;
                if (response.data.validate) {
                    ctrl.errores = false;
                    toastr.options.closeButton = true;
                    toastr.options.positionClass = "toast-bottom-center";
                    toastr.options.closeMethod = 'fadeOut';
                    toastr.success(response.data.message, 'Exito!',{timeOut: 7000});
                    setTimeout(function(){ $state.go('login'); }, 7000);
                } else{
                    ctrl.fail = response.data.message;
                    ctrl.errores = true;
                }
            }

            function error(response) {
                // ctrl.show_progress = false;
                // ctrl.password = null;
                // ctrl.fail = response.data.message;
                 ctrl.errores = true;
                toastr.options.closeButton = true;
                toastr.options.positionClass = "toast-bottom-center";
                toastr.options.closeMethod = 'fadeOut';
                toastr.success(response.data.message, 'Exito!',{timeOut: 7000});
                setTimeout(function(){ $state.go('login'); }, 7000);
            }
        }
    }
})();